'use strict';

function main(
  datasetId = 'my_new_dataset1', 
  tableId = 'my_new_table12345', 
  schema = [
    {name: 'id', type: 'INT64', mode: 'REQUIRED'},
    {name: 'user_id', type: 'INT64'},
    {name: 'api_user_id ', type: 'INT64'},
    {name: 'username ', type: 'STRING'},
    {name: 'merchantType ', type: 'STRING'},
    {name: 'add_info1 ', type: 'STRING'},
    {name: 'is_sl', type: 'BOOLEAN'},
    {name: 'amount', type: 'FLOAT64'},
    {name: 'previousAmount', type: 'STRING'},
    {name: 'balanceAmount', type: 'STRING'},
    {name: 'operation_performed', type: 'STRING'},
    {name: 'transaction_type', type: 'STRING'},
    {name: 'transaction_status_code', type: 'FLOAT64'},
    {name: 'status_desc', type: 'STRING'},
    {name: 'status', type: 'STRING'},
    {name: 'created_date', type: 'TIMESTAMP'},
    {name: 'updated_date', type: 'TIMESTAMP'},
    {name: 'rrn', type:'STRING'},
    {name: 'npic_trans_id', type: 'STRING'},
    {name: 'upi_trans_ref_no', type: 'STRING'},
    {name: 'param_b', type: 'STRING'},
    {name: 'param_c', type: 'STRING'},
    {name: 'add_info2', type: 'STRING'},
    {name: 'user_role', type: 'STRING'},
    {name: 'service_provider_id', type: 'INT64'},
    {name: 'api_service_provider_id', type: 'INT64'},
    {name: 'lat_long', type: 'STRING'},
  ]
) {
  
  const {BigQuery} = require('@google-cloud/bigquery');
  const bigquery = new BigQuery({ projectId: "test-project-297909",keyFilename:"key.json"});




  async function createTable() {
    // Creates a new table 

    var d = new Date();
    
    var date = d.getDate();
    if(date>0 && date<10)
      date=  "0".concat(date);
      var month = d.getMonth();
      var year = d.getFullYear();
      //var minute = d.getMinutes();
    var tablename1="transaction_status_"+year+  +month+  +date ;
     console.log("tablename",tablename1);
    const datasetId = "my_new_dataset1";
     const tableId = tablename1;
     const schema = 'id:integer,user_id:integer,api_user_id:integer,username:string,merchantType:string,add_info2:string,is_sl:boolean,amount:float,previousAmount:string,balanceAmount:string,operation_performed:string,transaction_type:string,transaction_status_code:float,status_desc:string,status:string,created_date:timestamp,updated_date:timestamp,rrn:string,npic_trans_id:string,upi_trans_ref_no:string,param_b:string,param_c:string,add_infow2:string,user_role:string,service_provider_id:integer,api_service_provider_id:integer,lat_lang:string';

    // For all options, see https://cloud.google.com/bigquery/docs/reference/v2/tables#resource
    const options = {
      schema: schema,
      location: 'US',
    };

    // Create a new table in the dataset
    const [table] = await bigquery
      .dataset(datasetId)
      .createTable(tableId, options);

    console.log(`Table ${table.id} created.`);
  }
  // [END bigquery_create_table]
  createTable();
}
main(...process.argv.slice(2));